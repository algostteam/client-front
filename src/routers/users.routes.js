const router = require('express').Router();

router.get('/me', (req, res) => {
  res.render('users/pages/me.pug');
});

module.exports = router;

const express = require('express');
const path = require('path');
const http = require('http');
const pug = require('pug');
const config = require('./config');
const log = require('./helpers/logger')(module);

const users = require('./routers/users.routes');

const app = express();

// Set view engine

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', pug);

// Set middleware

app.use(express.static(path.join(__dirname, 'public')));

// Routes

app.use('/users', users);

// 404
app.use((req, res) => {
  res.status(404).send({
    message: 'Page not found',
  });
});

// Express error handling
app.use((err, req, res) => {
  res.status(500).json({
    message: err.message,
  });
});

// Node.js error handling
process.on('unhandledRejection', (err) => {
  if (err) {
    log.error(err);
  }
});

process.on('rejectionHandled', (err) => {
  if (err) {
    log.error(err);
  }
});

process.on('uncaughtException', (err) => {
  if (err) {
    log.error(err);
  }
});

app.listen(config.get('port'), config.get('host'), () => {
  log.info(config.get('port'));
  log.info(`Server start on ${http.globalAgent.protocol}//${config.get('host')}:${config.get('port')}`);
});

module.exports = app;
